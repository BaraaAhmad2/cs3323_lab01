# CS3323_LAB01 PART 2


# Statement of Work
Baraa: Part 2.2 expr_list, Part 2.5 a_fact  
Josiah: Part 2.1 varlist, Part 2.6 control flow constructs    
Andrew: Part 2.3 l_fact, Part 2.4 varref

# Schedule
Baraa Part 2.2 and 2.5 - 11/22/2022 to 11/28/2022  
Josiah Part 2.1 and 2.6 - 11/22/2022 to 11/28/2022  
Andrew Part 2.3 and 2.4 - 11/22/2022 to 11/28/2022  
Everyone Testing - 11/28/2022 to 12/4/2022

![Part 2 Schedule](/part_ii/part_2_schedule.png "Part 2 Schedule")

# Meeting Minutes/Notes

## Meeting 1
Josiah finished parts 2.1 and 2.2, updated Baraa and Andrew

## Meeting 2
November 28, 11:30am-12:20pm (50 minutes)  
Josiah, Andrew and Baraa tested 2.1-2.5, taking note of any cases that needed to be fixed.

## Meeting 3
November  29, 6-7pm.  (60 minutes)  
More debugging and testing, got 2.1-2.5 fixed up.  

## Meeting 4
November 30, 11:30am-12:30pm (60 minutes)  
Josiah finalized 2.4-2.6, Andrew and Baraa did more testing.

## Meeting 5
December 1, 11:30am, 1pm (90 minutes)
Finished the project, fixed 2.6, tested the remaindeer of the exercises.

## Point Distribution

We collectively agreed to do a 33.3% point distribution for each member. Baraa 33.3%, Josiah 33.3%, and Andrew 33.3%.

## Rubric 

| Grader | README | Test Cases | P1 | P2 | P3 | P4 | P5 | P6 |
| ------ | ------ | ---------- | -- | -- | -- | -- | -- | -- |
| Team   |   4    |      6     |  2 |  2 |  6 | 4  | 8  | 8  |
| Peer   |        |            |    |    |    |    |    |    |
| Inst.  |        |            |    |    |    |    |    |    |



