# CS3323_LAB01 PART 1



# Statement of Work
Baraa: Part 1.1 Arithmetic Operators Rules, Part 1.3 L_Float Rule and Keywords  
Josiah: Part 1.2 T_ID Token Rule, Part 1.3 L_Float and Keywords   
Andrew: Test Case File, Testing, and Bug Fixes 

# Schedule

Baraa Part 1.1 - 11/7/2022 to 11/11/2022  
Josiah Part 1.2 - 11/7/2022 to 11/11/2022  
Baraa and Josiah Part 1.3 - 11/11/2022 to 11/18/2022  
Andrew Test Case File, Testing, and Bug Fixes - 11/18/2022 to 11/21/2022  

![Part 1 Schedule](/part_i/part_1_schedule.png "Part 1 Schedule")

# Meeting Minutes/Notes

## Meeting 1
November 8, 4pm-6pm (120 minutes)  
Finished #1 and #2 for Part 1. commits shown on github.  
Josiah and Baraa worked on analyzing the regex and token files provided, and relying on the test cases to determine if the output is right. 

## Meeting 2
November  10, 6-7pm.  (60 minutes)  
Started Part II. Andrew worked on test cases for part I, Josiah and Baraa and Andrew continued brainstorming for Part II.  

## Meeting 3
Novebmer 11, 11:30am-12:30pm (60 minutes)  
Josiah and Baraa finalized part I, confirmed it with Dr. Veras.  
Andrew also looked at part I and II.  

## Point Distribution

We collectively agreed to do a 33.3% point distribution for each member. Baraa 33.3%, Josiah 33.3%, and Andrew 33.3%.

## Rubric 

| Grader | README | Test Cases | P1 | P2 | P3 | P4 |
| ------ | ------ | ---------- | -- | -- | -- | -- |
| Team   |   4    |      6     |  6 |  8 | 10 | 6  |
| Peer   |        |            |    |    |    |    |
| Inst.  |        |            |    |    |    |    |



