# CS3323_LAB01



# Statement of Work

## Part 1
Baraa: Part 1.1 Arithmetic Operators Rules, Part 1.3 L_Float Rule and Keywords  
Josiah: Part 1.2 T_ID Token Rule, Part 1.3 L_Float and Keywords   
Andrew: Test Case File  


# Lab Schedule

## Part 1 Schedule 
Baraa Part 1.1 - 11/7/2022 to 11/11/2022  
Josiah Part 1.2 - 11/7/2022 to 11/11/2022  
Baraa and Josiah Part 1.3 - 11/11/2022 to 11/18/2022  
Andrew Test Case File - 11/18/2022 to 11/20/2022  

![Part 1 Schedule](/part_i/part_1_schedule.png "Part 1 Schedule")

# Meeting Minutes/Notes 

## Part 1 Meetings 

### Meeting 1
November 8, 4pm-6pm (120 minutes)  
Finished #1 and #2 for Part 1. commits shown on github.  
Josiah and Baraa worked on analyzing the regex and token files provided, and relying on the test cases to determine if the output is right. 

### Meeting 2
November  10, 6-7pm.  (60 minutes)  
Started Part II. Andrew worked on test cases for part I, Josiah and Baraa and Andrew continued brainstorming for Part II.  

### Meeting 3
Novebmer 11, 11:30am-12:30pm (60 minutes)  
Josiah and Baraa finalized part I, confirmed it with Dr. Veras.  
Andrew also looked at part I and II. 

## Point Distribution

We collectively agreed to do a 33.3% point distribution for each member. Baraa 33.3%, Josiah 33.3%, and Andrew 33.3%.

# Rubric 

## Part 1

| Grader | README | Test Cases | P1 | P2 | P3 | P4 |
| ------ | ------ | ---------- | -- | -- | -- | -- |
| Team   |   4    |      6     |  6 |  8 | 10 | 6  |
| Peer   |        |            |    |    |    |    |
| Inst.  |        |            |    |    |    |    |

## Part 2
Josiah: 2.1-2.3 11/24 to 11/28
Andrew: 2.4, 2.6 11/28 to 11/30
Baraa: 2.2, 2.5  11/28 to 11/30

## Part 2 Meetings 

### Meeting 1
Josiah finished parts 2.1 and 2.2, updated Baraa and Andrew

### Meeting 2
November 28, 11:30am-12:20pm (50 minutes)  
Josiah, Andrew and Baraa tested 2.1-2.5, taking note of any cases that needed to be fixed.

### Meeting 3
November  29, 6-7pm.  (60 minutes)  
More debugging and testing, got 2.1-2.5 fixed up.  

### Meeting 4
November 30, 11:30am-12:30pm (60 minutes)  
Josiah finalized 2.4-2.6, Andrew and Baraa did more testing.

### Meeting 5
December 1, 11:30am, 1pm (90 minutes)
Finished the project, fixed 2.6, tested the remaindeer of the exercises.

## Point Distribution

We collectively agreed to do a 33.3% point distribution for each member. Baraa 33.3%, Josiah 33.3%, and Andrew 33.3%.

# Rubric 

## Part 2

| Grader | README | Test Cases | P1 | P2 | P3 | P4 |
| ------ | ------ | ---------- | -- | -- | -- | -- |
| Team   |       |           |   |   |  |   |
| Peer   |        |            |    |    |    |    |
| Inst.  |        |            |    |    |    |    |



